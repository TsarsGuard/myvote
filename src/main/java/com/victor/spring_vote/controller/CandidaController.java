package com.victor.spring_vote.controller;

import com.victor.spring_vote.entity.Candida;
import com.victor.spring_vote.service.inf.ICandidaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Controller
public class CandidaController {

	@Resource(name = "candidaServiceImpl")
	private ICandidaService canService;

	@RequestMapping(value = "/addCandida", method = RequestMethod.POST)
	public String vote(@RequestParam(required = false) String candida,
                       HttpServletRequest request, Model model) {
		Candida can = new Candida();
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/******/
		String time = sdf.format(d);
		can.setName(candida);
		can.setTime(time);
		canService.insert(can);
		return "redirect/votesuccess";
	}

	@RequestMapping(value = "/showCandida", method = RequestMethod.POST)
	public String showCandida(@RequestParam(required = false) String candida,
                              HttpServletRequest request, Model model) {
		ArrayList<Candida> list = (ArrayList<Candida>) canService.show();
		model.addAttribute("c_list", list);
		return "redirect/candida";
	}

}
