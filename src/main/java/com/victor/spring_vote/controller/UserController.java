package com.victor.spring_vote.controller;

import com.victor.spring_vote.dao.IUserDao;
import com.victor.spring_vote.entity.User;
import com.victor.spring_vote.service.impl.IUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@Controller
public class UserController {

    @Resource(name = "iUserDao")
    public IUserDao UserDao;

    @Resource(name = "iUserServiceImpl")
    private IUserServiceImpl userService;

    /**
     * 登录
     *
     * @param username
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/test1", method = RequestMethod.POST)
    public String test(@RequestParam(required = false) String username,
                       HttpServletRequest request, Model model) {
        System.out.println("start");
        User u = UserDao.selectByName(username);
        if (u == null)
            return "error";
        if (!u.getPwd().equals(request.getParameter("password")))
            return "error";
        model.addAttribute("name", username);
//        UserUtil.setLoguser(u);
        System.out.println("end");
        return "redirect/showName";
    }

//    /**
//     * 添加新用户
//     *
//     * @param username
//     * @param request
//     * @param model
//     * @return
//     */
//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String add(@RequestParam(required = false) String username,
//                      HttpServletRequest request, Model model) {
//        // 判断是否为管理员
//        if (UserUtil.getLoguser().getId() == "1") {
//            User u = new User();
//            String one = request.getParameter("password");
//            String two = request.getParameter("password2");
//            if (one.equals(two)) {
//                u.setName(username);
//                u.setPwd(one);
//                userService.insert(u);
//                return "showName";
//            } else
//                return "error";
//        } else
//            return "/WEB-INF/jsp/" + "notAdmin.jsp";
//    }
    @RequestMapping(value = "/showname", method = RequestMethod.GET)
    public String showUserName(HttpServletRequest request, Model model) {

        return "redirect/showName";
    }
}
