package com.victor.spring_vote.dao;

import com.victor.spring_vote.entity.VoteResult;
import org.springframework.stereotype.Repository;
import com.victor.spring_vote.entity.Voter;

import java.util.List;

@Repository("iVoteDao")
public interface IVoteDao {
	void insert(Voter can);

	List<VoteResult> showResult();
}
