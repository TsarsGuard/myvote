package com.victor.spring_vote.dao;


import org.springframework.stereotype.Repository;
import com.victor.spring_vote.entity.User;

@Repository("iUserDao")
public interface IUserDao {
    int deleteByPrimaryKey(Integer id);

    void insert(User record);

    void update_flag(String id);

    User selectByName(String name);

    int updateByPrimaryKeySelective(User record);

    void updateById(User record);

    User getUser(int id);
}