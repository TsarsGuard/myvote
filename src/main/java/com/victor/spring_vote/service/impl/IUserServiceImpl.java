package com.victor.spring_vote.service.impl;

import com.victor.spring_vote.dao.IUserDao;
import org.springframework.stereotype.Service;
import com.victor.spring_vote.dao.IVoteDao;
import com.victor.spring_vote.entity.User;
import com.victor.spring_vote.service.inf.UserService;

import javax.annotation.Resource;

@Service("iUserServiceImpl")
public class IUserServiceImpl  implements UserService{

	@Resource(name = "iUserDao")
	private IUserDao udao;

	@Resource(name = "iVoteDao")
	private IVoteDao vdao;

	public void insert(User record) {
		udao.insert(record);
	}

	public void update_flag(String id) {
		udao.update_flag(id);
	}

	public User getUser(int id) {
		// TODO Auto-generated method stub
		return null;
	}



}
