package com.victor.spring_vote.service.impl;


import com.victor.spring_vote.dao.IUserDao;
import com.victor.spring_vote.entity.VoteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.victor.spring_vote.dao.IVoteDao;
import com.victor.spring_vote.entity.Voter;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VoteServiceImpl implements IVoteDao {

    @Resource(name = "iUserDao")
    private IUserDao udao;

    @Resource(name = "iVoteDao")
    private IVoteDao votedao;

    public void insert(Voter can) {
//		User u = UserUtil.getLoguser();
        votedao.insert(can);
//		udao.update_flag(u.getId());
    }

    public List<VoteResult> showResult() {

        return votedao.showResult();
    }

}
